<?php

namespace App\Libraries;

use App\Poll as PollModel;
use App\PollChoice;
use App\User;
use App\Vote;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;

class Poll
{

    public static function getStatus(PollModel $poll)
    {

        if (strtotime($poll->starts_at) > time()) {
            return [
                'contextual_helper' => 'primary',
                'label' => 'Upcoming'
            ];
        } else if (strtotime($poll->starts_at) <= time() && strtotime($poll->ends_at) > time()) {
            return [
                'contextual_helper' => 'success',
                'label' => 'On-going'
            ];
        } else {
            return [
                'contextual_helper' => 'danger',
                'label' => 'Closed'
            ];
        }

    }

    public static function generateFileName(UploadedFile $file)
    {
        return md5(rand()) . '_' . uniqid() . '.' . $file->clientExtension();
    }

    public static function storeMainImage(UploadedFile $file)
    {
        $fileName = self::generateFileName($file);

        $file->move(public_path('img/polls/main'), $fileName);

        return $fileName;
    }

    public static function storeCoverImage(UploadedFile $file)
    {
        $fileName = self::generateFileName($file);

        $file->move(public_path('img/polls/cover'), $fileName);

        return $fileName;
    }

    public static function storePollChoiceImage(UploadedFile $file)
    {
        $fileName = self::generateFileName($file);

        $file->move(public_path('img/polls/choices'), $fileName);

        return $fileName;
    }

    public static function sortPollChoicesRank(Collection $choices)
    {
        return $choices->sortByDesc(function ($choice) {
            return count($choice->votes);
        });
    }

    public static function calculateVotesRatio(Collection $choices)
    {

        if ($choices->isEmpty()) {
            return $choices;
        }
        $votes = [];

        foreach ($choices as $choice) {
            $votes[] = $choice->votes->count();
        }

        $base = max($votes);

        $choices = $choices->each(function ($choice, $key) use ($base) {

            if ($base == 0) {
                $choice['votes_ratio'] = number_format(0, 2);

            } else {
                $percentage = $choice->votes->count();

                $rate = ($percentage / $base) * 100;

                $choice['votes_ratio'] = number_format($rate, 2);
            }
            
        });

        return $choices;

    }

    public static function canVote(User $user, PollModel $poll)
    {
        // Check if the $user has already voted
        if (self::hasVoted($user, $poll)) {
            return false;
        }

        // Check if the $poll is on "On-going" state
        if (self::getStatus($poll)['label'] !== 'On-going') {
            return false;
        }

        return true;

    }

    /**
     * Returns true if the given user has already voted on the given poll, otherwise false
     *
     * @param User $user
     * @param PollModel $poll
     * @return bool
     */
    public static function hasVoted(User $user, PollModel $poll)
    {

        $choiceIds = PollChoice::where('poll_id', $poll->id)->pluck('id');

        return (bool)Vote::where('voter_id', $user->id)->whereIn('poll_choice_id', $choiceIds)->count();

    }

}