<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $fillable = ['voter_id', 'poll_choice_id'];

    public function voter() {
        return $this->belongsTo('App\User', 'voter_id');
    }

    public function pollChoice() {
        return $this->belongsTo('App\PollChoice');
    }
}
