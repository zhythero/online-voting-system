<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Vote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function index(Request $request)
    {

        $users = User::paginate($request->input('per_page', 20));

        return view('admin/users/list')->with(['users' => $users]);
    }

    public function create()
    {
        return view('admin/users/create');
    }

    public function edit(User $user)
    {
        return view('admin/users/edit')->with(['user' => $user]);
    }

    public function store(Request $request)
    {

        $this->validateCreateUser($request);

        $user = new User();

        $user->account_type = $request->input('account_type');
        $user->student_id = $request->input('student_id');
        $user->password = bcrypt($request->input('password'));
        $user->first_name = $request->input('first_name');
        $user->middle_name = $request->input('middle_name');
        $user->last_name = $request->input('last_name');

        $user->save();

        $request->session()->flash('success', 'Successfully created user');

        return redirect('admin/users');

    }

    public function update(User $user, Request $request)
    {

        $this->validateEditUser($user, $request);

        $user = User::find($user->id);

        $user->account_type = $request->input('account_type');
        $user->student_id = $request->input('student_id');
        $user->first_name = $request->input('first_name');
        $user->middle_name = $request->input('middle_name');
        $user->last_name = $request->input('last_name');

        $user->save();

        $request->session()->flash('success', 'Successfully edited user');

        return redirect('admin/users');

    }

    public function destroy(User $user, Request $request) {

        Vote::where('voter_id', $user->id)->delete();
        
        $user->delete();

        return redirect('/admin/users')->with('success', 'Successfully deleted user.');
    }

    public function validateCreateUser(Request $request)
    {
        $this->validate($request, [
            'account_type' => 'in:voter,admin',
            'student_id' => 'required|unique:users,student_id|max:100',
            'password' => 'required|confirmed|min:8|max:40',
            'first_name' => 'required|max:100',
            'middle_name' => 'max:100',
            'last_name' => 'required|max:100'
        ]);
    }

    public function validateEditUser(User $user, Request $request)
    {
        $this->validate($request, [
            'account_type' => 'in:voter,admin',
            'student_id' => 'required|unique:users,student_id,'.$user->id.'|max:100',
            'first_name' => 'required|max:100',
            'middle_name' => 'max:100',
            'last_name' => 'required|max:100'
        ]);
    }
}
