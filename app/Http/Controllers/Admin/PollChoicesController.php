<?php

namespace App\Http\Controllers\Admin;

use App\Poll;
use App\PollChoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\Poll as PollLibrary;

class PollChoicesController extends Controller
{
    public function index(Poll $poll)
    {
        return view('admin/polls/choices/list')
            ->with([
                'poll' => $poll,
                'choices' => PollChoice::where('poll_id', $poll->id)->get()
            ]);
    }

    public function create(Poll $poll)
    {
        return view('admin/polls/choices/create')->with(['poll' => $poll]);
    }

    public function store(Poll $poll, Request $request) {

        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'image' => 'required|image|dimensions:width=300,height=300'
        ]);

        PollChoice::create([
            'poll_id' => $poll->id,
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'image' => PollLibrary::storePollChoiceImage($request->file('image'))
        ]);

        return redirect('admin/polls/'.$poll->id.'/choices')
            ->with('success', 'Successfully created poll');
    }

    public function edit(Poll $poll, PollChoice $pollChoice)
    {
        return view('admin/polls/choices/edit')
            ->with(['poll' => $poll, 'pollChoice' => $pollChoice]);
    }

    public function update(Poll $poll, PollChoice $pollChoice, Request $request) {

        $this->validate($request, [
            'name' => 'required',
            'description' => 'required'
        ]);

        PollChoice::where('id', $pollChoice->id)->update([
            'poll_id' => $poll->id,
            'name' => $request->get('name'),
            'description' => $request->get('description')
        ]);

        if ($request->hasFile('image')) {
            $this->validate($request, [
                'image' => 'image|dimensions:width=300,height=300'
            ]);

            PollChoice::where('id', $pollChoice->id)->update([
                'image' => PollLibrary::storePollChoiceImage($request->file('image'))
            ]);
        }

        return redirect('admin/polls/'.$poll->id.'/choices')
            ->with('success', 'Successfully edited poll');
    }

    public function destroy(Poll $poll, PollChoice $pollChoice) {
        
        $pollChoice->delete();

        return redirect()->back()->with('success', "Successfully deleted choice");
    }
}
