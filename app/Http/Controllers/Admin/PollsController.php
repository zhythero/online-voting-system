<?php

namespace App\Http\Controllers\Admin;

use App\Poll;
use App\PollChoice;
use App\Http\Controllers\Admin\PollChoicesController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use App\Libraries\Poll as PollLibrary;

class PollsController extends Controller
{
    public function index()
    {
        $polls = Poll::all();

        return view('admin/polls/list')->with(['polls' => $polls]);
    }

    public function create()
    {
        return view('admin/polls/create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required|max:100',
            'description' => 'required|max:4000',
            'main_image' => 'required|image|dimensions:width=400,height=400',
            'cover_image' => 'required|image|dimensions:width=1920,height=300',
            'starts_at' => 'required|date',
            'ends_at' => 'required|date',
//            'max_choices' => 'required|integer'
        ]);

        Poll::create([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'starts_at' => date('Y-m-d H:i:s', strtotime($request->get('starts_at'))),
            'ends_at' => date('Y-m-d H:i:s', strtotime($request->get('ends_at'))),
            'max_choices' => 1,
//            'max_choices' => $request->get('max_choices'),
            'main_image' => PollLibrary::storeMainImage($request->file('main_image')),
            'cover_image' => PollLibrary::storeCoverImage($request->file('cover_image')),
            'created_by' => Auth::id()
        ]);

        $request->session()->flash('success', 'Successfully created poll');

        return redirect('admin/polls');

    }

    public function show(Poll $poll)
    {
        $choices = PollChoice::with('votes')->where('poll_id', $poll->id)->get();

        $choices = PollLibrary::sortPollChoicesRank($choices);

        $choices = PollLibrary::calculateVotesRatio($choices);

        return view('admin/polls/show')
            ->with(['poll' => $poll, 'choices' => $choices]);
    }

    public function edit(Poll $poll)
    {
        return view('admin/polls/edit')->with(['poll' => $poll]);
    }

    public function update(Poll $poll, Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:100',
            'description' => 'required|max:4000',
            'starts_at' => 'required|date',
            'ends_at' => 'required|date',
//            'max_choices' => 'required|integer'
        ]);

        $poll->update([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'starts_at' => date('Y-m-d H:i:s', strtotime($request->get('starts_at'))),
            'ends_at' => date('Y-m-d H:i:s', strtotime($request->get('ends_at'))),
//            'max_choices' => $request->get('max_choices'),
        ]);

        if ($request->hasFile('main_image')) {

            $this->validate($request, [
                'main_image' => 'image|dimensions:width=400,height=400',
            ]);

            $poll->update([
                'main_image' => PollLibrary::storeMainImage($request->file('main_image'))
            ]);

        }

        if ($request->hasFile('cover_image')) {
            $this->validate($request, [
                'cover_image' => 'image|dimensions:width=1920,height=300',
            ]);

            $poll->update([
                'cover_image' => PollLibrary::storeCoverImage($request->file('cover_image'))
            ]);
        }

        $request->session()->flash('success', 'Successfully updated poll');

        return redirect('admin/polls/' . $poll->id);
    }

    public function destroy(Poll $poll, Request $request)
    {

        $poll->delete();

        return redirect('admin/polls')->with('success', 'Successfully deleted poll');
    }
}
