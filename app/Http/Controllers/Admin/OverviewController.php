<?php

namespace App\Http\Controllers\Admin;

use App\PollChoice;
use App\Poll;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class OverviewController extends Controller
{
    public function index() {
    	$upcomingPolls = Poll::where('starts_at', '>', Carbon::now())->count();
        $onGoingPolls = Poll::where('starts_at', '<=', Carbon::now())->where('ends_at', '>', Carbon::now())->count();
        $closedPolls = Poll::where('ends_at', '<=', Carbon::now())->count();
        $voters = User::where('account_type', 'voter')->count();

    	return view('admin.overview')
    		->with([
                'upcomingPolls' => $upcomingPolls,
                'onGoingPolls' => $onGoingPolls,
                'closedPolls' => $closedPolls,
                'voters' => $voters,
            ]);
    }

    public function test(PollChoice $foo) {
        dd($foo->id);
    }
}
