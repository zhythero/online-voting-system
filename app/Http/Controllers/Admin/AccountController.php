<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;

class AccountController extends Controller
{
    
	public function index() {
		return view('admin/account')->with('me', Auth::user());
	}

	public function update(Request $request) {

		$this->validate($request, [
            'first_name' => 'required|max:100',
            'middle_name' => 'max:100',
            'last_name' => 'required|max:100'
        ]);
        $user = User::where('id', Auth::id())->get()->first();

        $user->student_id = $request->input('student_id');
        $user->first_name = $request->input('first_name');
        $user->middle_name = $request->input('middle_name');
        $user->last_name = $request->input('last_name');

        $user->save();

        return redirect()->back()->with('success', 'Successfully updated your account');

	}

	public function changePassword(Request $request) {

		$this->validate($request, [
            'password' => 'required|confirmed|min:8|max:40',
        ]);

		$user = User::where('id', Auth::id())->get()->first();

		$user->password = bcrypt($request->get('password'));

		$user->save();

		return redirect()->back()->with('success', 'Successfully changed password');
	}

}
