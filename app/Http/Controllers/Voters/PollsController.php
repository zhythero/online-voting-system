<?php

namespace App\Http\Controllers\Voters;

use App\Poll;
use App\PollChoice;
use App\Libraries\Poll as PollLibrary;
use App\Vote;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PollsController extends Controller
{
    public function index() {
        $upcomingPolls = Poll::where('starts_at', '>', Carbon::now())->get();
        $onGoingPolls = Poll::where('starts_at', '<=', Carbon::now())->where('ends_at', '>', Carbon::now())->get();
        $closedPolls = Poll::where('ends_at', '<=', Carbon::now())->get();

        return view('voters/index')
            ->with([
                'upcomingPolls' => $upcomingPolls,
                'onGoingPolls' => $onGoingPolls,
                'closedPolls' => $closedPolls,
            ]);
    }

    public function show(Poll $poll) {

        $choices = PollChoice::with('votes')->where('poll_id', $poll->id)->get();

        $poll['status'] = PollLibrary::getStatus($poll)['label'];
        $poll['can_vote'] = PollLibrary::canVote(Auth::user(), $poll);

        return view('voters/show')
            ->with([
                'poll' => $poll,
                'choices' => $choices
            ]);

    }

    public function vote(Poll $poll, Request $request) {

        if ( ! PollLibrary::canVote(Auth::user(), $poll) ) {
            return redirect()->back()->with('error', "You cannot vote for this poll.");
        }

        // TODO: Validate the poll_choice_id if it does really belong to $poll

        $this->validate($request, [
            'poll_choice_id' => 'exists:poll_choices,id'
        ]);

        Vote::create([
            'poll_choice_id' => $request->get('poll_choice_id'),
            'voter_id' => Auth::id()
        ]);

        return redirect()->back()->with('success', "Successfully voted!");
    }

}
