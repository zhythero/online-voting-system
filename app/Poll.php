<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Poll extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'title','description','main_image',
        'cover_image','max_choices','created_by',
        'starts_at','ends_at'
    ];

    public function choices() {
        return $this->hasMany('App\PollChoice');
    }

    public function votes() {
        return $this->hasManyThrough('App\Vote', 'App\PollChoice');
    }
}
