<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PollChoice extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $fillable = ['poll_id','name','description','image'];

    public function poll() {
        return $this->belongsTo('App\Poll');
    }

    public function votes() {
        return $this->hasMany('App\Vote');
    }
}
