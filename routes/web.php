<?php

/*
|--------------------------------------------------------------------------
| Authentication
|--------------------------------------------------------------------------
*/
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

/*
|--------------------------------------------------------------------------
| Voters
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => 'auth'], function() {
	Route::get('/', 'Voters\PollsController@index');
	Route::get('/polls/{poll}', 'Voters\PollsController@show');
	Route::post('/polls/{poll}/vote', 'Voters\PollsController@vote');
	Route::get('/home', 'Voters\PollsController@index');
});

/*
|--------------------------------------------------------------------------
| Admin Panel
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => '/admin', 'middleware' => 'admin'], function() {
	Route::get('/', 'Admin\OverviewController@index');
	Route::get('/overview', 'Admin\OverviewController@index');
	Route::get('/account', 'Admin\AccountController@index');
	Route::put('/account', 'Admin\AccountController@update');
	Route::patch('/account/password', 'Admin\AccountController@changePassword');
	Route::resource('/users', 'Admin\UsersController', ['except' => 'show']);
	Route::resource('/polls', 'Admin\PollsController');
	Route::resource('/polls.choices', 'Admin\PollChoicesController', [
		'parameters' => [
			'choices' => 'pollChoice'
		]
	]);
});
