@extends('layouts.admin')

@section('content')
	
	<h2>Account Settings</h2>

	<div class="row">
		<div class="col-md-12">
			@if (session('success'))
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<p>{{ session('success') }}</p>
				</div>
			@endif
		</div>
		<div class="col-md-6">
			<h3>Change Account Info</h3>
			<form method="POST" action="{{ url()->current() }}">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				<div class="form-group">
					<label>Username</label>
					<input class="form-control" value="{{ $me->student_id }}" type="text" name="student_id">
				</div>
				<div class="form-group">
					<label>First Name</label>
					<input class="form-control" value="{{ $me->first_name }}" type="text" name="first_name">
				</div>
				<div class="form-group">
					<label>Middle Name</label>
					<input class="form-control" value="{{ $me->middle_name }}" type="text" name="middle_name">
				</div>
				<div class="form-group">
					<label>Last Name</label>
					<input class="form-control" value="{{ $me->last_name }}" type="text" name="last_name">
				</div>
				<button class="btn btn-primary">Save</button>
			</form>
		</div>
		<div class="col-md-6">
			<h3>Change Password</h3>
			<form method="POST" action="{{ url()->current() .'/password' }}">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
				<div class="form-group">
					<label>Password</label>
					<input class="form-control" type="password" name="password" placeholder="Type password here">
				</div>
				<div class="form-group">
					<label>Confirm Password</label>
					<input class="form-control" type="password" name="password_confirmation" placeholder="Type password again">
				</div>
				<button class="btn btn-primary">Save</button>
			</form>
		</div>
	</div>


@endsection