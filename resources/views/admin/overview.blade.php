@extends('layouts.admin')

@section('content')

	<h2 class="text-center">Overview</h2>

	<hr>

	<div class="row">
		<div class="col-md-12">
			<div class="row text-center">
				<div class="col-md-4">
					<h1 id="on-going">2</h1>
					<h2 class="text-muted">On-going Polls</h2>
				</div>
				<div class="col-md-4">
					<h1 id="upcoming">0</h1>
					<h2 class="text-muted">Upcoming Polls</h2>
				</div>
				<div class="col-md-4">
					<h1 id="closed">2</h1>
					<h2 class="text-muted">Closed Polls</h2>
				</div>
			</div>
		</div>
		
		<div class="col-md-12 text-center">
			<hr>
			<h3>There are currently <span id="voters">0</span> voters.</h3>
		</div>
	</div>

@endsection

@push('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#on-going').animateNumber({ number: {{ $onGoingPolls }} });
		$('#upcoming').animateNumber({ number: {{ $upcomingPolls }} });
		$('#closed').animateNumber({ number: {{ $closedPolls }} });
		$('#voters').animateNumber({ number: {{ $voters }} });
	});
</script>
@endpush