@extends('layouts.admin')

@section('content')

	<h3>Polls</h3>
	<div class="row" style="padding-bottom: 10px">
		<div class="col-md-10">
			@if (session('success'))
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<p>{{ session('success') }}</p>
				</div>
			@endif
		</div>
		<div class="col-md-2">
			<a href="{{ url('admin/polls/create') }}" class="btn btn-primary pull-right">Create poll</a>
		</div>
	</div>
	<div class="row">
		<div class="container-fluid">
			<div class="row">
				@foreach($polls as $poll)
					<div class="col-md-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<a href="{{ url('/admin/polls/'.$poll->id) }}">Poll # {{ $poll->id }}</a>
							</div>
							<div class="panel-body">
								<img src="{{ url('/img/polls/main/'.$poll->main_image) }}" class="img-responsive" alt="">
								<h4 class="text-center">{{ $poll->title }}</h4>
								<p class="text-center">
									<span class="text-{{ App\Libraries\Poll::getStatus($poll)['contextual_helper'] }}">
										{{ App\Libraries\Poll::getStatus($poll)['label'] }}
									</span>
								</p>
								<p class="text-muted"><small>Start:</small></p>
								<p>
									{{ (new \Carbon\Carbon($poll->starts_at))->diffForHumans() }} <br>
									{{ (new \Carbon\Carbon($poll->starts_at))->toDayDateTimeString() }}
								</p>
								<p class="text-muted"><small>End:</small></p>
								<p>
									{{ (new \Carbon\Carbon($poll->ends_at))->diffForHumans() }} <br>
									{{ (new \Carbon\Carbon($poll->ends_at))->toDayDateTimeString() }}
								</p>
							</div>
							<div class="panel-footer">
								<a href="{{ url('/admin/polls/'.$poll->id) }}" class="btn btn-sm btn-default">View</a>
								<a href="{{ url('/admin/polls/'.$poll->id).'/edit' }}" class="btn btn-sm btn-default">Edit</a>
								<a href="#." data-toggle="modal" data-target="#delete-poll-{{ $poll->id }}-modal" class="btn btn-sm btn-danger">Delete</a>
								<div class="modal fade" id="delete-poll-{{ $poll->id }}-modal">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												Confirm delete
											</div>
											<div class="modal-body">
												Are you sure you want to delete this poll?
											</div>
											<div class="modal-footer">
												<form action="{{ url('/admin/polls/'.$poll->id) }}" method="POST">
													{{ method_field('DELETE') }}
													{{ csrf_field() }}
													<button type="submit" class="btn btn-danger">Delete</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>

@endsection