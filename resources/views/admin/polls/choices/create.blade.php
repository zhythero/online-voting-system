@extends('layouts.admin')

@push('css')
<style>
	.poll-main-image {
		margin: -130px 0 0 20px;
		width: 200px;
	}
</style>
@endpush

@section('content')

	<img class="img-responsive poll-cover-image" src="{{ url('img/polls/cover/'.$poll->cover_image) }}" alt="">
	<img class="img-responsive thumbnail poll-main-image" src="{{ url('img/polls/main/'.$poll->main_image) }}" alt="">
	<div class="row">
		<div class="col-md-8">
			@if (session('success'))
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<p>{{ session('success') }}</p>
				</div>
			@endif
			<h1>{{ $poll->title }}</h1>
			<h3 class="text-muted">Create new choice</h3>
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<form action="{{ url('admin/polls/'.$poll->id.'/choices') }}" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="media">
					<div class="media-left">
						<img id="poll-choice-image" src="{{ url('img/polls/choices/default.png') }}" alt="" class="media-object">
					</div>
					<div class="media-body">
						<div class="form-group">
							<label for="">Name</label>
							<input type="text" class="form-control input-sm" name="name">
						</div>
						<div class="form-group">
							<label for="">Description</label>
							<textarea class="form-control" name="description" id="" cols="30" rows="10"></textarea>
						</div>
						<div class="form-group">
							<label for="">Image</label>
							<p class="help-block">Must be 300x300</p>
							<input type="file" name="image" class="previewable" data-preview-target="#poll-choice-image">
						</div>
						<button type="submit" class="btn btn-primary">Save</button>
						<button class="btn btn-default">Cancel</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-md-4">
			<h3 class="text-muted">Description</h3>
			<p class="with-newline">{{ $poll->description }}</p>
			<h3 class="text-muted">Start:</h3>
			<p>{{ (new \Carbon\Carbon($poll->starts_at))->toDayDateTimeString() }}</p>
			<h3 class="text-muted">End:</h3>
			<p>{{ (new \Carbon\Carbon($poll->ends_at))->toDayDateTimeString() }}</p>
		</div>
	</div>
@endsection