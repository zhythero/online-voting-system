@extends('layouts.admin')

@push('css')
<style>
	.poll-main-image {
		margin: -130px 0 0 20px;
		width: 200px;
	}

	.with-newline {
		white-space: pre-line;
	}
</style>
@endpush

@section('content')

	<img class="img-responsive poll-cover-image" src="{{ url('img/polls/cover/'.$poll->cover_image) }}" alt="">
	<img class="img-responsive thumbnail poll-main-image" src="{{ url('img/polls/main/'.$poll->main_image) }}" alt="">
	<div class="row">
		<div class="col-md-8">
			@if (session('success'))
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<p>{{ session('success') }}</p>
				</div>
			@endif
			<h1>{{ $poll->title }}</h1>
			<h3 class="text-muted">
				Choices
				<a href="{{ url()->current() . '/create' }}" class="btn btn-primary">Add choice</a>
			</h3>
			@foreach($choices as $choice)
				<div class="media">
					<div class="media-left">
						<img src="{{ url('img/polls/choices/'.$choice->image) }}" alt="" class="media-object">
					</div>
					<div class="media-body">
						<h3 class="media-heading">{{ $choice->name }}</h3>
						<p class="with-newline">{{ $choice->description }}</p>
						<form class="form-inline" action="{{ url()->current() . '/' . $choice->id }}" method="POST">
							{{ csrf_field() }}
							{{ method_field('DELETE') }}
							<a href="{{ url()->current() . '/' . $choice->id . '/edit' }}" type="submit" class="btn btn-primary">Edit</a>
							<button type="submit" class="btn btn-danger">Delete</button>
						</form>
					</div>
				</div>
			@endforeach
		</div>
		<div class="col-md-4">
			<h3 class="text-muted">Description</h3>
			<p class="with-newline">{{ $poll->description }}</p>
			<h3 class="text-muted">Start:</h3>
			<p>{{ (new \Carbon\Carbon($poll->starts_at))->toDayDateTimeString() }}</p>
			<h3 class="text-muted">End:</h3>
			<p>{{ (new \Carbon\Carbon($poll->ends_at))->toDayDateTimeString() }}</p>
		</div>
	</div>
@endsection