@extends('layouts.admin')

@push('css')
<style>
    .poll-main-image {
        margin: -130px 0 0 20px;
        width: 200px;
    }

    .with-newline {
        white-space: pre-line;
    }

    .media-object {
        width: 100px;
    }
</style>
@endpush

@section('content')

    <img class="img-responsive poll-cover-image" src="{{ url('img/polls/cover/'.$poll->cover_image) }}" alt="">
    <img class="img-responsive thumbnail poll-main-image" src="{{ url('img/polls/main/'.$poll->main_image) }}" alt="">
    <div class="row">
        <div class="col-md-8">
            @if (session('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <p>{{ session('success') }}</p>
                </div>
            @endif
            <h1>
                {{ $poll->title }}
                <a href="{{ url()->current() . '/edit' }}" class="btn btn-primary">Edit</a>
                <br>
                <small>
                    <span class="text-{{ App\Libraries\Poll::getStatus($poll)['contextual_helper'] }}">
                        {{ App\Libraries\Poll::getStatus($poll)['label'] }}
                    </span>
                </small>
            </h1>
            <p class="with-newline">{{ $poll->description }}</p>
        </div>
        <div class="col-md-4">
            <h3>Start:</h3>
            <p>{{ (new \Carbon\Carbon($poll->starts_at))->toDayDateTimeString() }}</p>
            <h3>End:</h3>
            <p>{{ (new \Carbon\Carbon($poll->ends_at))->toDayDateTimeString() }}</p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-muted">
                Choices and Results
                <a href="{{ url()->current() . '/choices' }}" class="btn btn-primary">Manage choices</a>
            </h2>
            <div class="row">
                @foreach($choices as $choice)
                    <div class="col-md-6">
                        <div class="media">
                            <div class="media-left">
                                <img src="{{ url('img/polls/choices/'.$choice->image) }}" alt="" class="media-object">
                            </div>
                            <div class="media-body">
                                <h3 class="media-heading">{{ $choice->name }}</h3>
                                <div class="progress">
                                    <div class="progress-bar" style="width: {{ $choice->votes_ratio }}%">
                                        {{ $choice->votes->count() }} Votes
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection