@extends('layouts.admin')

@push('css')
<style>
	.poll-main-image {
		margin: -130px 0 0 20px;
		width: 200px;
	}

	.with-newline {
		white-space: pre-line;
	}
</style>
@endpush

@section('content')

	<img id="cover_image_preview" class="img-responsive poll-cover-image" src="{{ url('img/polls/cover/'.$poll->cover_image) }}" alt="">
	<img id="main_image_preview" class="img-responsive thumbnail poll-main-image" src="{{ url('img/polls/main/'.$poll->main_image) }}" alt="">
	<form action="{{ url('admin/polls/'.$poll->id) }}" method="POST" enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('PUT')}}
		<div class="row">
			<div class="col-md-8">
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				<br>
				<div class="form-group">
					<label for="">New main image</label>
					<input type="file" name="main_image" id="main_image">
				</div>
				<div class="form-group">
					<label for="">New cover image</label>
					<input type="file" name="cover_image" id="cover_image">
				</div>
				<div class="form-group">
					<label for="">Title</label>
					<input type="text" value="{{ $poll->title }}" name="title" class="form-control">
				</div>
				<div class="form-group">
					<label for="">Description</label>
					<textarea name="description" cols="30" rows="10" class="form-control">{{ $poll->description }}</textarea>
				</div>
				<div>
					<button type="submit" class="btn btn-primary">Save</button>
					<a href="{{ url('admin/polls/'.$poll->id) }}" class="btn btn-default">Cancel</a>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="">Starts</label>
					<div class="input-group date date-time-picker">
						<input type="text" class="form-control" value="{{ (new \Carbon\Carbon($poll->starts_at))->format('m/d/Y h:i A') }}"
								name="starts_at" placeholder="The date and time that the poll starts">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div>
				<div class="form-group">
					<label for="">Ends</label>
					<div class="input-group date date-time-picker">
						<input type="text" class="form-control" value="{{ (new \Carbon\Carbon($poll->ends_at))->format('m/d/Y h:i A') }}"
								name="ends_at" placeholder="The date and time that the poll ends">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div>
			</div>
		</div>
	</form>

@endsection

@push('scripts')
<script>
	function readURL(input, $target) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$target.attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	$(document).ready(function() {
		$("#main_image").change(function(){
			readURL(this, $('#main_image_preview'));
		});

		$("#cover_image").change(function(){
			readURL(this, $('#cover_image_preview'));
		});

		$('.date-time-picker').each(function() {
			$(this).datetimepicker();
		});
	});

</script>
@endpush