@extends('layouts.admin')

@section('content')

	<p><a href="{{ url('/admin/polls') }}">Go back</a></p>
	<h1>Create poll</h1>
	<div class="row">
		<form action="{{ url('admin/polls') }}" method="POST" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="col-md-12">
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
			<div class="col-md-4">
				<h2>Poll details</h2>
				<div class="form-group">
					<label for="">Title</label>
					<input type="text" class="form-control" name="title" placeholder="Poll title">
				</div>
				<div class="form-group">
					<label for="">Description</label>
					<textarea name="description" placeholder="Type description..." class="form-control" id="" cols="30" rows="10"></textarea>
				</div>
				<div class="form-group">
					<label for="">Starts</label>
					<div class="input-group date date-time-picker">
						<input type="text" class="form-control" name="starts_at" placeholder="The date and time that the poll starts">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div>
				<div class="form-group">
					<label for="">Ends</label>
					<div class="input-group date date-time-picker">
						<input type="text" class="form-control" name="ends_at" placeholder="The date and time that the poll ends">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div>
				{{--<div class="form-group">--}}
					{{--<label for="">Maximum choices a voter can have</label>--}}
					{{--<input type="number" class="form-control" name="max_choices" placeholder="Usually 1">--}}
				{{--</div>--}}
				<button type="submit" class="btn btn-primary">Create</button>
			</div>
			<div class="col-md-4">
				<h2>Poll images</h2>
				<div class="form-group">
					<label for="">Main Image</label>
					<img id="main-image" class="img-responsive" src="{{ url('img/polls/main/default.png') }}" alt="">
					<p class="help-block">The main image must be 400 x 400px in size</p>
					<input type="file" class="previewable" data-preview-target="#main-image" name="main_image">
				</div>
				<div class="form-group">
					<label for="">Cover Image</label>
					<img id="cover-image" class="img-responsive" src="{{ url('img/polls/cover/default.png') }}" alt="">
					<p class="help-block">The cover image must be 1920 x 300px in size</p>
					<input type="file" class="previewable" data-preview-target="#cover-image" name="cover_image">
				</div>
			</div>
		</form>
	</div>

@endsection