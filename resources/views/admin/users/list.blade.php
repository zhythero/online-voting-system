@extends('layouts.admin')

@section('content')

	<h3>Users</h3>
	<div class="row">
		<div class="col-md-10">
			@if (session('success'))
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<p>{{ session('success') }}</p>
				</div>
			@endif
		</div>
		<div class="col-md-2">
			<a href="{{ url('admin/users/create') }}" class="btn btn-primary pull-right">Create</a>
		</div>
	</div>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Student ID</th>
				<th>First</th>
				<th>Middle</th>
				<th>Last</th>
				<th>Role</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($users as $user)
				<tr class="{{ (Auth::id() == $user->id) ? 'success' : '' }}">
					<td>{{ $user->id }}</td>
					<td>{{ $user->student_id }}</td>
					<td>{{ $user->first_name }}</td>
					<td>{{ $user->middle_name }}</td>
					<td>{{ $user->last_name }}</td>
					<td>{{ $user->account_type }}</td>
					<td>
						@if (Auth::id() != $user->id)
							<a href="{{ url('admin/users/'.$user->id.'/edit') }}">Edit</a> |
							<a class="text-danger" href="#." data-toggle="modal" data-target="#delete-user-{{ $user->id }}-modal">Delete</a>
							<div class="modal fade" id="delete-user-{{ $user->id }}-modal">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											Confirm delete
										</div>
										<div class="modal-body">
											Are you sure you want to delete this user?
										</div>
										<div class="modal-footer">
											<form action="{{ url('/admin/users/'.$user->id) }}" method="POST">
												{{ method_field('DELETE') }}
												{{ csrf_field() }}
												<button type="submit" class="btn btn-danger">Delete</button>
											</form>
										</div>
									</div>
								</div>
							</div>
						@else

						@endif
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	<div class="text-center">
		{{ $users->links() }}
	</div>

@endsection