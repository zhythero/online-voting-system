@extends('layouts.admin')

@section('content')

	<p><a href="{{ url('/admin/users') }}">Go back</a></p>
	<h3>Create a user</h3>
	<form action="{{ url('admin/users') }}" method="POST">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="">Account Type</label>
			<div class="radio">
				<label for="voter">
					<input type="radio" name="account_type" value="voter" id="voter" checked>
					Voter
				</label>
			</div>
			<div class="radio">
				<label for="admin">
					<input type="radio" name="account_type" value="admin" id="admin">
					Admin
				</label>
			</div>
		</div>
		<div class="form-group">
			<label for="">Student ID / Admin Username</label>
			<p class="help-block">Student ID for voters or Username for admins</p>
			<input class="form-control" placeholder="Student ID / Admin Username" type="text" name="student_id">
		</div>
		<div class="form-group">
			<label for="">Password</label>
			<input type="password" class="form-control" placeholder="Password" name="password">
		</div>
		<div class="form-group">
			<label for="">Confirm Password</label>
			<input type="password" class="form-control" placeholder="Type password again" name="password_confirmation">
		</div>
		<div class="form-group">
			<label for="">First Name</label>
			<input type="text" class="form-control" name="first_name" placeholder="First Name">
		</div>
		<div class="form-group">
			<label for="">Middle Name</label>
			<input type="text" class="form-control" name="middle_name" placeholder="(Optional) Middle Name">
		</div>
		<div class="form-group">
			<label for="">Last Name</label>
			<input type="text" class="form-control" name="last_name" placeholder="Last Name">
		</div>
		<button type="submit" class="btn btn-primary">Submit</button>
	</form>

@endsection