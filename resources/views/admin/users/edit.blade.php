@extends('layouts.admin')

@section('content')

	<p><a href="{{ url('/admin/users') }}">Go back</a></p>
	<h3>Edit user</h3>
	<form action="{{ url('admin/users/'.$user->id) }}" method="POST">
		{{ method_field('PUT') }}
		{{ csrf_field() }}
		<div class="form-group">
			<label for="">Account Type</label>
			<div class="radio">
				<label for="voter">
					<input type="radio" name="account_type" value="voter" id="voter" {{ ($user->account_type == 'voter') ? 'checked' : '' }}>
					Voter
				</label>
			</div>
			<div class="radio">
				<label for="admin">
					<input type="radio" name="account_type" value="admin" id="admin" {{ ($user->account_type == 'admin') ? 'checked' : '' }}>
					Admin
				</label>
			</div>
		</div>
		<div class="form-group">
			<label for="">Student ID / Admin Username</label>
			<p class="help-block">Student ID for voters or Username for admins</p>
			<input class="form-control" placeholder="Student ID / Admin Username" value="{{ $user->student_id }}" readonly type="text" name="student_id">
		</div>
		<div class="form-group">
			<label for="">First Name</label>
			<input value="{{ $user->first_name }}" type="text" class="form-control" name="first_name" placeholder="First Name">
		</div>
		<div class="form-group">
			<label for="">Middle Name</label>
			<input value="{{ $user->middle_name }}" type="text" class="form-control" name="middle_name" placeholder="(Optional) Middle Name">
		</div>
		<div class="form-group">
			<label for="">Last Name</label>
			<input value="{{ $user->last_name }}" type="text" class="form-control" name="last_name" placeholder="Last Name">
		</div>
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="/admin/users" type="submit" class="btn btn-default">Cancel</a>
	</form>

@endsection