<!DOCTYPE html>
<html>
    <head>
        <title>{{ config('app.name', 'Online Voting System') }}</title>

        <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('css/bootflat.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('css/app.css') }}">

        <style type="text/css">

            body {
                background-color: #ECF0F1;
            }

            .full-page {
                min-height: 90vh;
            }
            
            .media-object {
                width: 164px;
            }

            div.well {
                background-size: cover;
            }

            .forcely-inverted-text {
                background-color: rgba(0,0,0,0.8); 
                color: #bbb;
            }

            .text-light {
                color: #bbb;
            }

        </style>
    </head>
    <body>
        <div class="container full-page">
            <header>
                <h1>Online Voting System</h1>

                @if (Auth::check())
                    <div class="row">
                        <div class="col-md-8">
                            <nav class="nav nav-pills">
                                <li class="active"><a href="{{ url('/') }}">Polls</a></li>
                                @if (Auth::user()->account_type == 'admin')
                                <li><a href="{{ url('/admin') }}">Admin Panel</a></li>
                                @endif
                            </nav>
                        </div>
                        <div class="col-md-4 text-right">
                            <p>Logged in as {{ Auth::user()->first_name }} | <a href="{{ url('/logout') }}">Logout</a></p>
                        </div>
                    </div>
                @endif
            </header>
            @yield('content')
            <br>
        </div>
        <div class="footer">
            <div class="container text-light">
                <p class="text-center">
                    Online Voting System Copyright &copy; 2017 | This website is solely for education use only. | Developed by Team Bits with advise from <a href="http://facebook.com/devzhy">Dev Zhy</a>.
                </p>
            </div>
        </div>
    </body>
</html>