<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ url('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/bootstrap-datetimepicker.min.css') }}">

    <style>
        .bordered-right {
            border-right: 1px solid #eee;
            min-height: 100vh;
        }

        body {
            background-color: #f5f5f5;
        }

        .with-newline {
            white-space: pre-line;
        }

        .media-object {
            width: 150px;
        }

        .progress .progress-bar {
            min-width: 4em;
        }
    </style>
    @stack('css')

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 bordered-right">
                <h3>Online Voting System <br><small>Admin Panel</small></h3>
                <hr>
                <ul class="nav nav-pills nav-stacked">
                    <li class="{{ (Request::is('admin/overview') || Request::is('admin/overview/*')) ? 'active' : '' }}">
                        <a href="{{ url('/admin/overview') }}">Overview</a>
                    </li>
                    <li class="{{ (Request::is('admin/users') || Request::is('admin/users/*')) ? 'active' : '' }}">
                        <a href="{{ url('/admin/users') }}">Users</a>
                    </li>
                    <li class="{{ (Request::is('admin/polls') || Request::is('admin/polls/*')) ? 'active' : '' }}">
                        <a href="{{ url('/admin/polls') }}">Polls</a>
                    </li>
                </ul>
                <hr>
                <p>Logged in as <a href="{{ url('admin/account') }}">{{ Auth::user()->first_name }}</a></p>
                <form action="{{ url('/logout') }}" method="POST">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-default btn-sm">Logout</button>
                </form>
            </div>
            <div class="col-md-10">
                @yield('content')

                <br>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ url('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ url('js/moment.min.js') }}"></script>
    <script src="{{ url('js/bootstrap.min.js') }}"></script>
    <script src="{{ url('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ url('js/animate-number.min.js') }}"></script>
    <script src="{{ url('js/admin.js') }}"></script>
    @stack('scripts')
</body>
</html>
