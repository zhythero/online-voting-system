@extends('layouts.voters')

@section('content')
    <h2>{{ $poll->title }}</h2>
    <p class="with-newline">{{ $poll->description }}</p>

    @if ($poll->can_vote)
        <div class="alert alert-success">
            <p>You have not voted for this poll yet. Vote now!</p>
        </div>
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            <p>{{ session('success') }}</p>
        </div>
    @endif

    <div class="row">
        @foreach ($choices as $choice)
            <div class="col-md-4">
                <div class="thumbnail">
                    <img class="img-responsive" src="{{ url('img/polls/choices/'.$choice->image) }}" alt="">
                    <div class="caption">
                        <h3>{{ $choice->name }}</h3>
                        <p class="with-newline">{{ $choice->description }}</p>
                        @if ($poll->status != 'Upcoming')
                            <h5>Current Votes: {{ $choice->votes->count() }}</h5>
                        @endif
                        @if ($poll->can_vote)
                            <form action="{{ url()->current() . '/vote' }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="poll_choice_id" value="{{ $choice->id }}">
                                <button class="btn btn-primary">Vote for {{ $choice->name }}</button>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>


@endsection