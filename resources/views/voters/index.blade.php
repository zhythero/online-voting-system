@extends('layouts.voters')

@section('content')

    <h3>On-going Polls</h3>
    @if ($onGoingPolls->count() == 0)
        <p class="text-info">There are currently no on-going polls.</p>
    @endif
    @foreach($onGoingPolls as $poll)
        <a href="{{ url('polls/'.$poll->id) }}">
            <div class="well" style="background-image: url('{{ url('img/polls/cover/'.$poll->cover_image) }}')">
                <div class="media">
                    <div class="media-left">
                        <img class="media-object" src="{{ url('img/polls/main/'.$poll->main_image) }}">
                    </div>
                    <div class="media-body">
                        <h1>
                            <span class="forcely-inverted-text">{{ $poll->title }}</span><br>
                            <small class="forcely-inverted-text">
                                Voting closes {{ (new \Carbon\Carbon($poll->ends_at))->diffForHumans() }}
                            </small>
                        </h1>
                    </div>
                </div>
            </div>
        </a>
    @endforeach

    <hr>

    <h3>Upcoming Polls</h3>
    @if ($upcomingPolls->count() == 0)
        <p class="text-info">There are currently no upcoming polls.</p>
    @endif
    @foreach($upcomingPolls as $poll)
        <a href="{{ url('polls/'.$poll->id) }}">
            <div class="well" style="background-image: url('{{ url('img/polls/cover/'.$poll->cover_image) }}')">
                <div class="media">
                    <div class="media-left">
                        <img class="media-object" src="{{ url('img/polls/main/'.$poll->main_image) }}">
                    </div>
                    <div class="media-body">
                        <h1>
                            <span class="forcely-inverted-text">{{ $poll->title }}</span><br>
                            <small class="forcely-inverted-text">
                                Will start {{ (new \Carbon\Carbon($poll->ends_at))->diffForHumans() }} ({{ (new \Carbon\Carbon($poll->starts_at))->toDayDateTimeString() }})
                            </small>
                        </h1>
                    </div>
                </div>
            </div>
        </a>
    @endforeach

    <hr>

    <h3>Closed Polls</h3>
    @if ($closedPolls->count() == 0)
        <p class="text-info">There are no closed polls.</p>
    @endif
    @foreach($closedPolls as $poll)
        <a href="{{ url('polls/'.$poll->id) }}">
            <div class="well" style="background-image: url('{{ url('img/polls/cover/'.$poll->cover_image) }}')">
                <div class="media">
                    <div class="media-left">
                        <img class="media-object" src="{{ url('img/polls/main/'.$poll->main_image) }}">
                    </div>
                    <div class="media-body">
                        <h1>
                            <span class="forcely-inverted-text">{{ $poll->title }}</span><br>
                            <small class="forcely-inverted-text">
                                Closed at {{ (new \Carbon\Carbon($poll->starts_at))->toDayDateTimeString() }}
                            </small>
                        </h1>
                    </div>
                </div>
            </div>
        </a>
    @endforeach
@endsection