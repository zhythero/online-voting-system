function readFile(input, target) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(target).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$(document).ready(function() {

    $(".previewable").each(function() {
        $(this).change(function(){
            readFile(this, $(this).attr('data-preview-target'));
        });
    });

    $('.date-time-picker').each(function() {
        $(this).datetimepicker();
    });
});