<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('student_id');
            $table->string('password');
            $table->enum('account_type', ['voter','admin']);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        User::create([
            'first_name' => 'Administrator',
            'middle_name' => '',
            'last_name' => 'Account',
            'student_id' => 'admin',
            'password' => bcrypt('password'),
            'account_type' => 'admin'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
